#!/usr/bin/env perl
use v5.24;
use warnings;
use English qw< -no_match_vars >;
use experimental 'signatures';
no warnings 'experimental::signatures';

my $filename = shift // 'vocabolario-di-base-input.txt';

my $text = do {
   open my $fh, '<:encoding(Latin1)', $filename
      or die "open('$filename'): $OS_ERROR\n";
   local $/;
   <$fh>;
};

$text =~ s{^\s*[A-Z]\s*[\r\n]*}{, }gmxs;
$text =~ s{ -[\r\n] }{}gmxs;
$text =~ s{ [\r\n]+ }{ }gmxs;

binmode STDOUT, ':encoding(UTF-8)';
my $last = '';
for (split m{ , \s* }mxs, $text) {
   my ($word) = m{\A \d* (\w+) \s+\S}mxs or next;
   say $word if $word ne $last;
   $last = $word;
}

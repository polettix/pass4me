#!/usr/bin/env perl
use v5.24;
use warnings;
use experimental 'signatures';
no warnings 'experimental::signatures';
use Scalar::Util qw< refaddr >;
use List::Util   qw< first max >;

use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5";

use Cpanel::JSON::XS qw< decode_json >;
use Path::Tiny;
use Log::Log4perl::Tiny qw< :easy LOGLEVEL >;

$|++;
binmode STDOUT, ':encoding(UTF-8)';

my $startfile = shift // 'parole-utf8.txt';
my $verbsfile = shift // 'verbi.json';
my $spmffile  = shift // 'spmf.txt';

my $basedir = path($RealBin);

LOGLEVEL('DEBUG');
LOGLEVEL('INFO');

INFO "initializing sets";
my $set_for = {map { lc($_) => {lc($_) => 1} }
     path($startfile)->lines_utf8({chomp => 1}),};
INFO scalar(keys $set_for->%*), ' starting words/sets';

INFO "expanding verbs";
load_verb_expansions($set_for, $verbsfile);
INFO scalar(keys $set_for->%*), ' sets';

INFO "expanding singular/plural/female/male";
load_spmf_expansions($set_for, $spmffile);
INFO scalar(keys $set_for->%*), ' sets';
#say Cpanel::JSON::XS->new->canonical->ascii->pretty->encode($set_for);

INFO 'grouping';
my $groups = sets_to_groups($set_for);
INFO scalar($groups->@*), ' raw groups';

INFO 'compactifying';
$groups = compactify($groups,
   sub ($word) { my $l = length $word; 4 <= $l && $l <= 6 }, 4);

INFO scalar($groups->@*), ' compact groups';
say Cpanel::JSON::XS->new->canonical->ascii->pretty->encode($groups);

sub sets_to_groups ($sets) {
   my @groups;
   my %seen;
   for my $set (values $set_for->%*) {
      next if $seen{refaddr($set)}++;

      my @words = sort { $a cmp $b }
        grep { !m{[^a-z]}mxs } keys $set->%*;
      next unless @words;

      push @groups, \@words;
   } ## end for my $set (values $set_for...)
   return \@groups;
}

sub load_verb_expansions ($set_for, $filename) {
   state $is_interesting_group = {
      map { $_ => 1 }
        qw<
        infinitive
        gerund
        presentparticiple
        pastparticiple
        indicative/present
        indicative/imperfect
        incative/pasthistoric
        indicative/future
        >
   };
   my $record_for = decode_json(path($filename)->slurp_raw);

   for my $word (keys $set_for->%*) {
      DEBUG "verb<$word>";
      my $record = $record_for->{$word} or next;
      my @words  = grep { !m{\W}mxs }
        map  { split m{(,\s*)+}mxs, $_->{value} =~ s{\A\s+|\s+\z}{}rgmxs }
        grep { $is_interesting_group->{$_->{group}} }
        $record->{conjugations}->@*;
      DEBUG " --> words(@words)";
      add_related($set_for, $word, @words) if @words;
   } ## end for my $word (keys $set_for...)

   return $set_for;
} ## end sub load_verb_expansions

sub load_spmf_expansions ($set_for, $filename) {
   for my $line (path($filename)->lines_utf8) {
      $line =~ s{\A\s+|\s+\z}{}gmxs;

      my ($main, @pairs) = split m{;}mxs, $line;

      my %kv = map {
         my ($key, @values) = map { lc }
           grep { length $_ && $_ =~ m{[a-z]}imxs }
           map { s{\A\s+|\s+\z}{}rgmxs } split m{[=,]}mxs, $_;
         $key => \@values;
      } grep { /=/ } @pairs;

      my @additional;
      for my $key (qw< pf sf pm sm >) {
         my $values = $kv{$key} or next;
         push @additional, $values->@*;
      }

      my @matches = grep { exists $set_for->{$_} } ($main, @additional)
        or next;

      # duplicates are OK
      add_related($set_for, @matches, $main, @additional);
   } ## end for my $line (path($filename...))
   return $set_for;
} ## end sub load_spmf_expansions

sub add_related ($set_for, $first, @other_words) {
   $first = lc $first;
   my $root = $set_for->{$first} // die "WTF?!?";
   DEBUG "   add_related: starting from <$first><$root>";
   for my $w (@other_words) {
      my $word = lc $w;
      DEBUG "   add_related: analysing <$word>";
      if (my $wroot = $set_for->{$word}) {
         DEBUG "   add_related:    wroot<$wroot>";
         next if refaddr($root) eq refaddr($wroot);    # already merged
         ($root, $wroot) = ($wroot, $root) # swap if...
           if scalar(keys $root->%*) < scalar(keys $wroot->%*);
         for my $item (keys $wroot->%*) {
            $root->{$item}    = 1;
            $set_for->{$item} = $root;
         }
      } ## end if (my $wroot = $set_for...)
      else {
         DEBUG "   add_related: NEW SET";
         $set_for->{$word} = $root;
         $root->{$word}    = 1;
      }
   } ## end for my $w (@other_words)
} ## end sub add_related

sub related_to ($word, $set_for) {
   $word = $set_for->{$word} while defined($word) && !ref($word);
   return $word;
}

sub compactify ($groups, $is_good, $threshold = 4) {
   my %good_groups;
   for my $group ($groups->@*) {
      my $n_good = grep { $is_good->($_) } $group->@* or next;
      push $good_groups{$n_good}->@*, [$group->@*];
   }

   my $max = max(keys %good_groups);
   for my $base_size (reverse(1 .. ($threshold - 1))) {
      while ($good_groups{$base_size}->@* > 0) {
         my @new_group      = shift($good_groups{$base_size}->@*)->@*;
         my $new_group_size = $base_size;
         while ($new_group_size < 4) {
            my $companion_size =
              first { ($good_groups{$_} // [])->@* > 0 } 1 .. $max;
            my $companion = shift $good_groups{$companion_size}->@*;
            push @new_group, $companion->@*;
            $new_group_size += $companion_size;
         } ## end while ($new_group_size < ...)
         push $good_groups{$new_group_size}->@*, \@new_group;
         $max = $new_group_size if $max < $new_group_size;
      } ## end while ($good_groups{$base_size...})
   } ## end for my $base_size (reverse...)

   return [map { $_->@* } values %good_groups];
} ## end sub compactify

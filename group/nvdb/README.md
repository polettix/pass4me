# Nuovo vocabolario di base della lingua italiana

These files allow generating groups of words for Italian.

The starting file is `vocabolario-di-base-input.txt`, which is the
textual representation of [Nuovo vocabolario di base][].

Words are extracted using `parole.pl` to generate `parole-utf8.txt`, see
[Extract words from the NVdB PDF file][wordlist].

Groups are generated from it using `expand.pl`. Start from [Expanding
words in NVdB][] for the details.

File `verbi.json.gz` is compressed and must be decompressed before being
used by `expand.pl`. Its content have been assembled from [ian-hamlin/verb-data][].

File `spmf.txt.gz` is compressed and must be decompressed before being
used by `expand.pl`. Its content have been assembled from a dump of the
[Wiktionary][], available [here][]. The extraction has been performed
using program `spmf.pl`.

The code parts are Copyright 2022 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[wordlist]: http://etoobusy/2022/10/16/nvdb-wordlist/
[Nuovo vocabolario di base]: https://www.internazionale.it/opinione/tullio-de-mauro/2016/12/23/il-nuovo-vocabolario-di-base-della-lingua-italiana
[Expanding words in NVdB]: http://etoobusy/2022/10/17/nvdb-expansions/
[ian-hamlin/verb-data]: https://github.com/ian-hamlin/verb-data
[Wiktionary]: https://en.wiktionary.org/wiki/Wiktionary:Main_Page
[here]: https://dumps.wikimedia.org/itwiktionary/

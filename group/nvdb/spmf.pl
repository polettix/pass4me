#!/usr/bin/env perl
use v5.24;
use warnings;
use experimental 'signatures';
no warnings 'experimental::signatures';

use XML::Twig;

binmode STDOUT, ':encoding(UTF-8)';
$|++;

my $filename = shift // '../itwiktionary-20220820-pages-meta-current.xml';
my $language = shift // 'it';

my $twig = XML::Twig->new(
   pretty_print => 'indented',
   twig_handlers => { page => page_handler_factory($language) },
);
$twig->parsefile($filename);

sub page_handler_factory ($target_language = 'it') {
   return sub ($twig, $page_elt) {
      state $seen = 0;
      state $tests = [\&tabs, \&linkp, \&agg_4, \&agg_2, \&gender];
      state $section_extractor_for = {
         #agg  => $tests,
         #sost => $tests,
         agg  => \&parse_agg,
         sost => \&parse_sost,
      };

      say {*STDERR} $seen if ++$seen % 1000 == 0;
      #exit 0 if $seen > 900;

      if (my $title_elt = $page_elt->first_child('title')) {
         if ((my $title = $title_elt->text) !~ m{:}mxs) {
            for my $text_elt ($page_elt->descendants('text')) {
               my @rows = split m{\n+}mxs, $text_elt->text;

               # look for right section based on language
               my $sections = sections_for(\@rows, $target_language)
                  or next;

               for my $section ($sections->@*) {
                  my ($type, $rows) = $section->@{qw< type rows >};
                  my $extractor = $section_extractor_for->{$type} or next;
                  my @pairs = $extractor->($title, $section) or next;
                  my @printable_pairs;
                  while (@pairs) {
                     my ($key, $value) = splice @pairs, 0, 2;
                     push @printable_pairs, "$key=$value";
                  }
                  say join ';', $title, @printable_pairs;
               }
            }
exit if $title =~ m{\A marted. \z}mxs;
         }
         else { return }
      }
      $page_elt->purge;
      return;
   };
}

sub parse_sost ($main, $section) {
   my $rows = $section->{rows};
   my @retval;

   my @gender = alts_cb($rows, \&gender);
   @gender = (gender => 'm,f') unless @gender;
   push @retval, @gender;

   if (my @tabs = alts_cb($rows, \&tabs)) {
      push @retval, @tabs;
   }
   elsif (my (undef, @plurals) = alts_cb($rows, \&linkp)) {
      my $plural = join ',', @plurals;
      my %items = map { ("s$_" => $main, "p$_" => $plural) }
         grep { $gender[1] =~ m{$_} } qw< f m >;
      push @retval, sorthash(%items);
   }
   else {
      my %items = map { ("s$_" => $main, "p$_" => $main) }
         grep { $gender[1] =~ m{$_} } qw< f m >;
      push @retval, sorthash(%items);
   }

   return (type => sost => @retval);
}

sub parse_agg ($main, $section) {
   my $rows = $section->{rows};
   my @retval;

   my @gender = alts_cb($rows, \&gender);
   @gender = (gender => 'm,f') unless @gender;
   push @retval, @gender;

   if (my @agg_4 = alts_cb($rows, \&agg_4)) {
      push @retval, @agg_4;
   }
   elsif (my @agg_2 = alts_cb($rows, \&agg_2)) {
      push @retval, @agg_2;
   }
   elsif (my @tabs = alts_cb($rows, \&tabs)) {
      push @retval, @tabs;
   }
   elsif (my (undef, @plurals) = alts_cb($rows, \&linkp)) {
      my $plural = join ',', @plurals;
      my %items = map { ("s$_" => $main, "p$_" => $plural) }
         grep { $gender[1] =~ m{$_} } qw< f m >;
      push @retval, sorthash(%items);
   }
   else {
      my %items = map { ("s$_" => $main, "p$_" => $main) }
         grep { $gender[1] =~ m{$_} } qw< f m >;
      push @retval, sorthash(%items);
   }

   return (type => agg => @retval);
}

sub alts_rx ($rows, $regex) {
   for my $row ($rows->@*) {
      my @matches = $row =~ $regex or next;
      return (@matches);
   }
   return;
}

sub alts_cb ($rows, $sub) {
   for my $row ($rows->@*) {
      my @matches = $sub->($row) or next;
      return @matches;
   }
   return;
}

sub alts ($main, $rows, $filter) {
   return alts_rx($main, $rows, $filter) if ref($filter) eq 'Regexp';
   return alts_cb($main, $rows, $filter) if ref($filter) eq 'CODE';
   ...;
}

sub sections_for ($rows, $target_language) {
   my ($start, $end) = rows_for($rows, $target_language) or return;
   return divide_subsections($rows, $start, $end);
}

sub divide_subsections ($rows, $start, $end) {
   my @sections;
   for my $i ($start .. $end) {
      if ($rows->[$i] =~ m{\A\s* \{\{-(.*?)-}mxs) {
         push @sections, { type => $1, rows => [] };
      }
      elsif (@sections) {
         push $sections[-1]{rows}->@*, $rows->[$i];
      }
   }
   return \@sections;
}

sub rows_for ($rows, $target_language) {
   state $language_delimiter_rx =
      qr{(?mxs:\A\s* == \s* \{\{-(.+?)-\}\} \s* == \s*\z)};

   my $i = 0; # index in array $rows->@*

   # look for start-of-section
   while ($i <= $rows->$#*) {
      if (my ($language) = $rows->[$i] =~ $language_delimiter_rx) {
         last if $language eq $target_language;
      }
      ++$i;
   }

   return if $i > $rows->$#*; # nothing found

   # save the start of the section
   my $start_i = $i;

   # look for end-of-section
   ++$i; # start from following line
   while ($i <= $rows->$#*) {
      last if $rows->[$i] =~ $language_delimiter_rx; # any will do
      ++$i;
   }

   return ($start_i, $i - 1); # delimiters for the section
}

sub gender ($string) {
   my @genders = $string =~ m{''\s* ([fm]) (?: \s+ .*?)?''}gmxs or return;
   return(gender => join ',', @genders);
}

sub sorthash (%h) { map { $_ => $h{$_} } sort { $a cmp $b } keys %h }

sub agg_4 ($string) {
   my ($exp) = $string =~ m{\{\{It-decl-agg4\|(.*)\}\}}imxs
      or return;
   my ($root, $sm, $pm, $sf, $pf, $sa) = grep {!/=/} split m{\|}mxs, $exp;
   $sm ||= 'o';
   $pm ||= 'i';
   $sf ||= 'a';
   $pf ||= 'e';
   $sa ||= 'issim';

   my %retval;
   @retval{qw< sm pm sf pf >} = map { $root . $_ } ($sm, $pm, $sf, $pf);
   @retval{qw< smsup pmsup sfsup pfsup >}
      = map { $root . $sa . $_ } ($sm, $pm, $sf, $pf);
   return sorthash(%retval);
}

sub agg_2 ($string) {
   my ($exp) = $string =~ m{\{\{It-decl-agg2\|(.*)\}\}}imxs
      or return;
   my ($root, $s, $p, $sa) = grep {!/=/} split m{\|}mxs, $exp;
   $s ||= 'e';
   $p ||= 'i';
   $sa ||= 'issim';
   my %retval;
   @retval{qw< sm pm >} = @retval{qw< sf pf >} = map { $root . $_ } ($s, $p);
   @retval{qw< smsup pmsup sfsup pfsup >}
      = map { $root . $sa . $_ } qw< o i a e >;
   return sorthash(%retval);
}

sub linkp ($string) {
   my ($plural) = $string =~ m{\{\{Linkp\|(.*?)\}\}}imxs or return;
   return (plural => split m{\|}mxs, $plural);
}

sub tabs ($string) {
   my ($forms) = $string =~ m{\{\{Tabs\|(.*?)\}\}}imxs or return;
   my @fields = qw< sm pm sf pf >;
   my %field_for = qw< m sm mp pm f sf fp pf >;
   my @parts = split m{\|}mxs, $forms;
   my %retval;
   for my $i (0 .. $#parts) {
      if (my ($key, $value) = $parts[$i] =~ m{\A (.*?) = (.*) \z}mxs) {
         $retval{$field_for{$key}} = $value if exists $field_for{$key};
      }
      else {
         $retval{$fields[$i]} = $parts[$i];
      }
   }
   return sorthash(%retval);
}

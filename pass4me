#!/usr/bin/env perl
# vim: ts=3 sts=3 sw=3 et ai :
# pass4me - Generate passphrases
use v5.24;
use warnings;
use experimental 'signatures';
no warnings 'experimental::signatures';
use autodie;
use Cwd 'realpath';
use File::Spec;
use JSON::PP 'decode_json';
use List::Util qw< shuffle uniq >;
use Pod::Usage qw< pod2usage >;
use Getopt::Long qw< GetOptionsFromArray :config gnu_getopt >;
use English qw< -no_match_vars >;
my $VERSION = '0.1';

my $config = get_options(
   [
      {
         getopt => 'long=i',
         default => 7,
         environment => 'PASS4ME_LONG',
      },
      {
         getopt => 'max=i',
         default => 9,
         environment => 'PASS4ME_MAX',
      },
      {
         getopt => 'min=i',
         default => 3,
         environment => 'PASS4ME_MIN',
      },
      {
         getopt => 'n_parts|n-parts=i',
         default => 4,
         environment => 'PASS4ME_N_PARTS',
      },
      {
         getopt => 'random_int|random-int=i',
         default => 10,
         environment => 'PASS4ME_RANDOM_INT',
      },
      {
         getopt => 'separator=s',
         default => '.',
         environment => 'PASS4ME_SEPARATOR',
      },
      {
         getopt => 'short=i',
         default => 3,
         environment => 'PASS4ME_SHORT',
      },
      {
         getopt => 'uc_first|uc-first=i',
         default => 1,
         environment => 'PASS4ME_UC_FIRST',
      },
      {
         getopt => 'words_file|words-file=i',
         default => 'pass4me-groups.json',
         environment => 'PASS4ME_WORDS_FILE',
      },
   ],
   [@ARGV],
);

$config->{words_file} = real_sibling(__FILE__, $config->{words_file})
   unless -e $config->{words_file};
say gen_passphrase($config);

sub get_options ($specs, $ARGV) {
   my (%cmdline, %environment, %default);
   my @cmdline_options = qw< help! man! usage! version! >;
   for my $spec ($specs->@*) {
      my ($optnames, $default, $env_var) =
        ref $spec
        ? $spec->@{qw< getopt default environment >}
        : ($spec, undef, undef);
      push @cmdline_options, $optnames;
      (my $name = $optnames) =~ s{[^\w-].*}{}mxs;
      $default{$name}     = $default       if defined $default;
      $environment{$name} = $ENV{$env_var}
         if defined $env_var && defined $ENV{$env_var};
   } ## end for my $spec ($specs->@*)

   GetOptionsFromArray($ARGV, \%cmdline, @cmdline_options)
     or pod2usage(-verbose => 99, -sections => 'USAGE');

   pod2usage(message => "$0 $VERSION", -verbose => 99, -sections => ' ')
     if $cmdline{version};
   pod2usage(-verbose => 99, -sections => 'USAGE') if $cmdline{usage};
   pod2usage(-verbose => 99, -sections => 'USAGE|EXAMPLES|OPTIONS')
     if $cmdline{help};
   pod2usage(-verbose => 2) if $cmdline{man};

   return {%default, %environment, %cmdline};
} ## end sub get_options

sub real_sibling ($reference, $name) {
   my ($v, $ds, $f) = File::Spec->splitpath(realpath($reference));
   return File::Spec->catpath($v, $ds, $name);
}

sub load_json ($path) {
   open my $fh, '<:encoding(UTF-8)', $path or die "open('$path'): $!\n";
   local $/;
   return decode_json(<$fh>);
}

sub gen_passphrase ($config) {
   my $n = $config->{n_parts};
   my ($min, $short, $long, $max) = $config->@{qw< min short long max >};
   my $uc_first = $config->{uc_first};
   my $random_int = $config->{random_int};
   my $words_file = $config->{words_file};
   my $separator = $config->{separator} // '.';

   # words are arranged in sets, one per line, separated by spaces.
   # Shuffling all sets is a bit overkill but effective.
   my @sets = shuffle load_json($words_file)->@*;

   my %parts;
   my ($has_short, $has_long); # allow max 1 short & 1 long

   # iterate until we have enough parts
   while (@sets && scalar(keys %parts) < $n) {
      for my $candidate (shuffle shift(@sets)->@*) {
         next if $parts{$candidate}; # cope with duplicates, just in case
         my $len = length $candidate;
         next if $len < $min
            || $max < $len
            || ($len  <= $short && $has_short)
            || ($long <= $len   && $has_long);

         # We can use this word
         $parts{$candidate} = 1;

         # take notes of possible usage of boundary lengths
         $has_short = 1 if $len  <= $short;
         $has_long = 1  if $long <= $len;

         # no more from this set
         last;
      }
   }

   my @parts = keys %parts;
   die "not enough sets to choose from\n" if @parts < $n;

   $parts[0] = ucfirst($parts[0]) if $uc_first;

   # if we add a number, we insist that we start with a word anyway
   if ($random_int) {
      my $first = shift @parts;
      @parts = ($first, shuffle(@parts, int rand $random_int));
   }

   return join $separator, @parts;
}

__END__

=pod

=encoding utf-8

=head1 NAME

pass4me - Generate passphrases

=head1 VERSION

The version can be retrieved with option C<--version>:

   $ pass4me --version

=head1 USAGE

   pass4me [--help] [--man] [--usage] [--version]

   pass4me  [--long int]
            [--max int]
            [--min int]
            [--n-parts int]
            [--random-int int]
            [--separator string]
            [--short int]
            [--uc-first bool]
            [--word-file name]

=head1 EXAMPLES

   $ pass4me
   Manzo.ideate.6.riviste.campo

   $ pass4me --n-parts 6
   Allena.altra.elencati.stuoia.7.famose.sfere

   $ pass4me --n-parts 6 --random-int 1000
   Orsa.visive.inviavamo.matto.svelto.sardi.782

=head1 DESCRIPTION

Generate a password as a sequence of words drawn randomly from a file.

The file is encoded in JSON format and contains an array of arrays. Each
sub-array contains a list of "related" words and will be used at most once.

Words shorter than the C<min> or longer than the C<max> are excluded.

Words shorter than or equal to C<short> can be chosen at most once. Words
longer than or equal to C<long> can be chosen at most once too.

C<n-parts> words will be generated, defaulting to 4 words.

The first word's first letter is turned to uppercase according to
C<uc-first>.

A random integer is added to the lot. It's possible to set the value
beyond the maximum with C<random-int>. This can occur anywhere, except
as the first element.

Words are separated according to a C<separator>.

Word groups are taken from file provided through C<words-file>. The
Italian file provided in the repo has been generated:

=over

=item *

starting from Prof. Tullio De Mauro's Nuovo vocabolario di base della
lingua italiana

=item *

expanding verbs using L<https://github.com/ian-hamlin/verb-data>

=item *

expanding nouns and adjectives using L<https://en.wiktionary.org/wiki/Wiktionary:Main_Page>

=back

=head1 OPTIONS

=over

=item B<--help>

print out some help and exit.

=item B<--long>

no more than one word of this length or more can be included.

Defaults to 7.

=item B<--man>

show the manual page for pass4me.

=item B<--max>

maximum length of any word.

Defaults to 9.

=item B<--min>

minimum length of any word.

Defaults to 3.

=item B<--n-parts>

number of words to be included in the passphrase.

Defaults to 4.

=item B<--random-int>

Upper limit for generating a random integer.

Defaults to 10, meaning a single digit integer.

=item B<--separator>

Separator string put between any pair of items.

Defaults to a single dot C<.>.

=item B<--short>

no more than one word of this length or less can be included.

Defaults to 3.

=item B<--uc-first>

set the first letter of the first word to uppercase.

Defaults to true.

=item B<--usage>

show usage instructions.

=item B<--words-file>

name of file with word groups (JSON file with an array of arrays).

File is looked into the programs's installation folder if not present
from the current directory.

Defaults to C<pass4me-groups.json>.

=item B<--version>

show version.

=back

=head1 BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
L<http://codeberg.org/polettix/pass4me>.

=head1 AUTHOR

Flavio Poletti

=head1 LICENSE AND COPYRIGHT

Copyright 2022 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file C<LICENSE> in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=cut

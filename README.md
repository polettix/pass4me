# NAME

pass4me - Generate passphrases

# VERSION

The version can be retrieved with option `--version`:

    $ pass4me --version

# USAGE

    pass4me [--help] [--man] [--usage] [--version]

    pass4me  [--long int]
             [--max int]
             [--min int]
             [--n-parts int]
             [--random-int int]
             [--separator string]
             [--short int]
             [--uc-first bool]
             [--word-file name]

# EXAMPLES

    $ pass4me
    Manzo.ideate.6.riviste.campo

    $ pass4me --n-parts 6
    Allena.altra.elencati.stuoia.7.famose.sfere

    $ pass4me --n-parts 6 --random-int 1000
    Orsa.visive.inviavamo.matto.svelto.sardi.782

# DESCRIPTION

Generate a password as a sequence of words drawn randomly from a file.

The file is encoded in JSON format and contains an array of arrays. Each
sub-array contains a list of "related" words and will be used at most once.

Words shorter than the `min` or longer than the `max` are excluded.

Words shorter than or equal to `short` can be chosen at most once. Words
longer than or equal to `long` can be chosen at most once too.

`n-parts` words will be generated, defaulting to 4 words.

The first word's first letter is turned to uppercase according to
`uc-first`.

A random integer is added to the lot. It's possible to set the value
beyond the maximum with `random-int`. This can occur anywhere, except
as the first element.

Words are separated according to a `separator`.

Word groups are taken from file provided through `words-file`. The
Italian file provided in the repo has been generated:

- starting from Prof. Tullio De Mauro's Nuovo vocabolario di base della
lingua italiana
- expanding verbs using [https://github.com/ian-hamlin/verb-data](https://github.com/ian-hamlin/verb-data)
- expanding nouns and adjectives using [https://en.wiktionary.org/wiki/Wiktionary:Main\_Page](https://en.wiktionary.org/wiki/Wiktionary:Main_Page)

# OPTIONS

- **--help**

    print out some help and exit.

- **--long**

    no more than one word of this length or more can be included.

    Defaults to 7.

- **--man**

    show the manual page for pass4me.

- **--max**

    maximum length of any word.

    Defaults to 9.

- **--min**

    minimum length of any word.

    Defaults to 3.

- **--n-parts**

    number of words to be included in the passphrase.

    Defaults to 4.

- **--random-int**

    Upper limit for generating a random integer.

    Defaults to 10, meaning a single digit integer.

- **--separator**

    Separator string put between any pair of items.

    Defaults to a single dot `.`.

- **--short**

    no more than one word of this length or less can be included.

    Defaults to 3.

- **--uc-first**

    set the first letter of the first word to uppercase.

    Defaults to true.

- **--usage**

    show usage instructions.

- **--words-file**

    name of file with word groups (JSON file with an array of arrays).

    File is looked into the programs's installation folder if not present
    from the current directory.

    Defaults to `pass4me-groups.json`.

- **--version**

    show version.

# BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
[http://codeberg.org/polettix/pass4me](http://codeberg.org/polettix/pass4me).

# AUTHOR

Flavio Poletti

# LICENSE AND COPYRIGHT

Copyright 2022 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
